package jsoup.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
    @JsonProperty(value = "ID")
    private Integer id;
    @JsonProperty(value = "NAME")
    private String name;
    @JsonProperty(value = "PRICE")
    private String price;
    @JsonProperty(value = "IMAGE_URL")
    private String imageUrl;
    @JsonProperty(value = "DETAIL_URL")
    private String detailUrl;

    public Product() {
    }

    public Product(Integer id, String name, String price,String imageUrl,String detailUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imageUrl=imageUrl;
        this.detailUrl=detailUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }
}
