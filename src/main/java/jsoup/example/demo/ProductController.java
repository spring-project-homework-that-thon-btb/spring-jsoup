package jsoup.example.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


@RestController
@RequestMapping("/json")
public class ProductController {

    public static final List<Product> allProduct = new ArrayList<>();
    private static final AtomicInteger index = new AtomicInteger(0);

    @GetMapping
    public ResponseEntity<Map<String, Object>> index() {

        Map<String, Object> response = new HashMap<>();
        try {
            Document document = Jsoup.connect("https://www.khmer24.com/en/motorcycles.html").get();
            document.select(".item").forEach(a -> {
                allProduct.add(new Product(
                        index.incrementAndGet(),
                        a.getElementsByClass("title").text(),
                        a.getElementsByClass("price").text(),
                        a.getElementsByClass("img-cover").attr("src"),
                        a.getElementsByTag("a").attr("href")));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (allProduct.isEmpty()) {
            response.put("MSG", "unsuccess");
            response.put("CD", "WC0001");
        } else {
            response.put("MSG", "success");
            response.put("CD", "0000");
            response.put("DATA", allProduct);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
